class Course < ApplicationRecord

  include PublicActivity::Model
  tracked owner: Proc.new{ |controller, model| controller.current_user }

  # text trix
  has_rich_text :description

  #friendly
  extend FriendlyId
  friendly_id :title, use: :slugged

  # Relationship
  belongs_to :user
  has_many :lessons, dependent: :destroy
  has_many :enrollments

  # Validates
  validates :title, :short_description, :language, :level, :price, presence: true
  validates :description, presence: true, length: { :minimum => 5 }

  def to_s
    title
  end

  # LANGUAGE
  LANGUAGE = [:"English", :"Russian",:"Polish", :"Spanish", :"Italy", :"Hebrew"]
  def self.languages
    LANGUAGE.map { |language| [language, language]}
  end

  # LEVELS
  LEVEL = [:"Beginner", :"Intermediate", :"Advance", :"Expert", :"Guru"]
  def self.levels
    LEVEL.map {|level| [level , level]}
  end


  def bought(user)
    self.enrollments.where(user_id: [user.id], course_id: [self.id].empty?)
  end

  #friendly_id :generate_slug, use :slugged
  #def generate_slug
  #  require 'secureradom'
  #  @random_slug ||= persisted? friendly_id : SecureRandom.hex(4)
  #end

  #def to_s
  #  slug
  #end
end
