class Lesson < ApplicationRecord
  # FriendlyId
  extend FriendlyId
  friendly_id :title, use: :slugged

  has_rich_text :content
  belongs_to :course
  validates :title, :content, :course, presence: true

  include PublicActivity::Model
  tracked owner: Proc.new{ |controller, model| controller.current_user }


  def to_s
    title
  end
end
