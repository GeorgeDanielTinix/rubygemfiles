Rails.application.routes.draw do

  resources :enrollments
  resources :lessons
  
  devise_for :users

  resources :courses do
    resources :enrollments, only: [:new, :create]
  end
  
  resources :users, only: [:index, :edit, :show, :update]

  get 'home/activity'

  root to: 'home#index'
end
