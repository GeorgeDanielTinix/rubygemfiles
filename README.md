# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* RUBY VERSION
   ruby 2.7.1p83

* BUNDLED WITH
   2.1.4

* RAILS VERSION
* 6.0.3

* Configuration

## First of all
* Bundle
* bundle install

* DATABASE
    Sqlite 1.4

* Database initialization

* Run:
    rake db:setup
    rake db:migrate
    rake db:seed

## After that

* yarn upgrade
* yarn --check-files
* bin/webpack-dev-server

## Then in another tab
* $rails s

## Open your browser
* http://localhost:3000
